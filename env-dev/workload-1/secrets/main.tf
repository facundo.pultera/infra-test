module "ecs_secrets" {
  source       = "../../../module/secrets/"
  secret_name  = "DB_CREDENTIAL"
  description  = "db credential"
  secrets     = {
    "DB_NAME" = "example-db"
    "DB_PASS" = "123455"
  }
  tags = {
    Environment = "dev"
    Project     = "my-project"
  }
}
