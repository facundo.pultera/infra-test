variable "name" {
    type = string
}

variable "container_port" {
    type = number
}

variable "host_port" {
    type = number
}

variable "app_protocol" {
    type = string
}

variable "cpu" {
    type = number
}

variable "memory" {
    type = number
}

variable "requires_compatibilities" {
    type = list(string)
}

variable "protocol" {
    type = string
}

variable "region" {
    type = string
}

variable "arn_id" {
    type = string
}

variable "secret_manager_name" {
    type = string
}

variable "SECRET_DB_NAME" {
  type        = string
  description = "Database name secret"
  # No default value because we expect it to come from the environment variable
}

variable "SECRET_DB_PASS" {
  type        = string
  description = "Database password secret"
  # No default value because we expect it to come from the environment variable
}
