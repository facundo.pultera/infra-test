module "ecs_task_definition" {
  source = "../../../module/tasks-definition"

  # Variables related to the task definition
  name                     = var.name
  image                    = "${var.arn_id}.dkr.ecr.${var.region}.amazonaws.com/example-api"
  container_port           = var.container_port
  host_port                = var.host_port
  protocol                 = var.protocol
  port_name                = "${var.name}-${var.container_port}-${var.protocol}"
  app_protocol             = var.app_protocol
  log_group                = "/ecs/${var.name}"
  log_region               = var.region
  log_stream_prefix        = "ecs"
  cpu                      = var.cpu
  memory                   = var.memory
  requires_compatibilities = var.requires_compatibilities
  # Pass the secrets as module arguments
  environment_secrets = {
    DB_NAME = var.SECRET_DB_NAME
    DB_PASS = var.SECRET_DB_PASS
    # ... any other secrets
  }
}
