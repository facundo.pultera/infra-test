name                     = "services-api"
container_port           = 3003
host_port                = 3003
app_protocol             = "http"
cpu                      = "256"
memory                   = "512"
requires_compatibilities = ["FARGATE"]
protocol                 = "tcp"
region                   = "us-west-2"
arn_id                   = "090101523344"
secret_manager_name      = "my-ecs-secret-aY6SuM"