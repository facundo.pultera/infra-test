# ecs.tf

terraform {
  backend "http" {}
}

provider "aws" {
  region = "us-east-1"
}

module "ecs_services" {
    source = "../../../module/ecs/"

    environment = "dev"

    cluster_name = "my-default-cluster"

    common_tags = {
    environment = "dev"
    owner       = "my-team"
    }

    services = [
        {
        name             = "service-1"
        task_definition  = "my-task-definition:1"
        specific_tags    = {
            custom_tag = "value1"
        }
        },
        {
        name             = "service-2"
        task_definition  = "my-task-definition:2"
        specific_tags    = {
            custom_tag = "value2"
        }
        }
    ]
}