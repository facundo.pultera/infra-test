module "application_target_group" {
  source = "../../../module/target-group"
  target_group_name       = "${var.target_group_name}-tg"
  vpc_id                  = var.vpc_id
  target_group_port       = var.target_group_port
  target_group_protocol   = var.target_group_protocol
  health_check_path       = var.health_check_path
  tags                    = { Environment = "${var.environment}", Project = "${var.target_group_name}" }
}
