variable "target_group_name" {
    type = string
}

variable "vpc_id" {
    type = string
}

variable "target_group_port" {
    type = string
}

variable "target_group_protocol" {
    type = string
}

variable "health_check_path" {
    type = string
}

variable "environment" {
    type = string
}