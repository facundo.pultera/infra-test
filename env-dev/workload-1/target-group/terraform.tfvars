target_group_name       = "services-app"
target_group_port       = 80
target_group_protocol   = "HTTP"
vpc_id                  = "vpc-06b7fa04fa0c48223"
health_check_path       = "/health"
environment             = "dev"