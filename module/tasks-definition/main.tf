resource "aws_ecs_task_definition" "this" {
  family                   = var.name
  network_mode             = "awsvpc"
  requires_compatibilities = var.requires_compatibilities
  cpu                      = var.cpu
  memory                   = var.memory
  execution_role_arn       = aws_iam_role.execution_role.arn
  task_role_arn            = aws_iam_role.task_role.arn

  container_definitions = jsonencode([{
    name  = var.name
    image = var.image

    secrets = [for s in keys(var.environment_secrets) : {
      name      = s
      valueFrom = var.environment_secrets[s]
    }]

    logConfiguration = {
      logDriver = "awslogs",
      options = {
        "awslogs-group"         = var.log_group,
        "awslogs-region"        = var.log_region,
        "awslogs-stream-prefix" = var.log_stream_prefix,
        "awslogs-create-group"  = "true",
      }
    }

    portMappings = [{
      containerPort = var.container_port,
      hostPort      = var.host_port,
      protocol      = var.protocol,
      name          = var.port_name
    }]

    environment = [{
      name  = "APP_PROTOCOL",
      value = var.app_protocol
    }]
  }])
}

resource "aws_iam_role" "execution_role" {
  name = "execution_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role" "task_role" {
  name = "task_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ecs-tasks.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "execution_role_attachment" {
  role       = aws_iam_role.execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "task_role_attachment" {
  role       = aws_iam_role.task_role.name
  # Attach the AmazonECSTaskExecutionRolePolicy policy for the sake of demonstration
  # In practice, you would attach policies that grant the permissions your task needs
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

