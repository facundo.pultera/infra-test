variable "name" {
  description = "Name of the task"
  type        = string
}

variable "image" {
  description = "Docker image URL"
  type        = string
}

variable "container_port" {
  description = "Container port"
  type        = number
}

variable "host_port" {
  description = "Host port"
  type        = number
}

variable "protocol" {
  description = "Protocol"
  type        = string
}

variable "port_name" {
  description = "Name of the port mapping"
  type        = string
}

variable "app_protocol" {
  description = "Application level protocol"
  type        = string
}

variable "log_group" {
  description = "CloudWatch log group name"
  type        = string
}

variable "log_region" {
  description = "Region for CloudWatch logs"
  type        = string
}

variable "log_stream_prefix" {
  description = "CloudWatch log stream prefix"
  type        = string
}

variable "cpu" {
  description = "CPU units for the task"
  type        = string
}

variable "memory" {
  description = "Memory for the task"
  type        = string
}

variable "requires_compatibilities" {
  description = "Deployment type"
  type        = list(string)
}

# locals {
#   secrets_list = [
#     { name: "DB_NAME", valueFrom: var.environment_secrets },
#     { name: "DB_PASS", valueFrom: var.environment_secrets }
#   ]
# }
# variable "environment_secrets" {
#   type        = string
#   description = "Secrets from GitLab CI/CD"
# }

variable "environment_secrets" {
  type        = map(string)
  description = "Map of environment secrets"
  default     = {} # default to an empty map if not set
}

variable "SECRET_DB_NAME" {
  type        = string
  description = "Database name from secret"
  default     = "" # default to an empty string if not set
}

variable "SECRET_DB_PASS" {
  type        = string
  description = "Database password from secret"
  default     = "" # default to an empty string if not set
}

locals {
  secrets_list = [
    {
      name      = "DB_NAME"
      valueFrom = var.SECRET_DB_NAME
    },
    {
      name      = "DB_PASS"
      valueFrom = var.SECRET_DB_PASS
    },
    # add other secrets here
  ]
}
