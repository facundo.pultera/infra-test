variable "target_group_name" {
  description = "The name of the target group"
  type        = string
}

variable "target_group_port" {
  description = "The port on which targets receive traffic"
  type        = number
}

variable "target_group_protocol" {
  description = "The protocol to use for routing traffic to the targets"
  type        = string
}

variable "vpc_id" {
  description = "The identifier of the VPC in which the target group is located"
  type        = string
}

variable "health_check_enabled" {
  description = "Indicates whether health checks are enabled"
  type        = bool
  default     = true
}

variable "health_check_interval" {
  description = "The approximate amount of time between health checks"
  type        = number
  default     = 30
}

variable "health_check_path" {
  description = "The destination for the health check request"
  type        = string
  default     = "/"
}

variable "health_check_port" {
  description = "The port to use for the health check"
  type        = string
  default     = "traffic-port"
}

variable "healthy_threshold" {
  description = "The number of consecutive health checks successes required before considering an unhealthy target healthy"
  type        = number
  default     = 3
}

variable "unhealthy_threshold" {
  description = "The number of consecutive health check failures required before considering a target unhealthy"
  type        = number
  default     = 3
}

variable "health_check_timeout" {
  description = "The amount of time to wait before failing a health check request"
  type        = number
  default     = 5
}

variable "health_check_protocol" {
  description = "The protocol to use for health checks"
  type        = string
  default     = "HTTP"
}

variable "health_check_matcher" {
  description = "The HTTP codes to use when checking for a successful response from a target"
  type        = string
  default     = "200"
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default     = {}
}
