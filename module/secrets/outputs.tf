output "arn" {
  description = "The Amazon Resource Name (ARN) of the secret."
  value       = aws_secretsmanager_secret.ecs_secret.arn
}

output "id" {
  description = "The unique identifier of the created secret."
  value       = aws_secretsmanager_secret.ecs_secret.id
}


# You can use the AWS CLI to encrypt the secrets using a specific KMS key
aws kms encrypt --key-id alias/my-key-alias --plaintext "MySecretContent" --query CiphertextBlob --output text
