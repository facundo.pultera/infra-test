variable "secret_name" {
  description = "The name of the secret."
  type        = string
}

variable "description" {
  description = "The description of the secret."
  type        = string
  default     = ""
}

variable "kms_key_id" {
  description = "The KMS key id to encrypt the secrets."
  type        = string
  default     = null
}

variable "tags" {
  description = "The tags for the secret."
  type        = map(string)
  default     = {}
}

variable "secrets" {
  description = "A map of secret key-value pairs."
  type        = map(string)
  default     = {}
}