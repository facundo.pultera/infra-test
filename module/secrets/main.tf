resource "aws_secretsmanager_secret" "ecs_secret" {
  name        = var.secret_name
  description = var.description
  kms_key_id  = var.kms_key_id
  tags        = var.tags
}

resource "aws_secretsmanager_secret_version" "ecs_secret_version" {
  secret_id     = aws_secretsmanager_secret.ecs_secret.id
  secret_string = jsonencode(var.secrets)
}