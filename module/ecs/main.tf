# module/ecs/main.tf

variable "common_tags" {
  type        = map(string)
  description = "Common Tags"
}

variable "environment" {
  type        = string
  description = "Environment (prd, dev, etc.)"
}

variable "services" {
  type        = list(object({
    name             = string
    task_definition = string
    specific_tags    = map(string)
  }))
  description = "List of ECS services to create"
}

resource "aws_ecs_service" "ecs_services" {
  for_each = { for service in var.services : service.name => service }

  name             = each.value.name
  cluster          = var.cluster_name
  task_definition  = each.value.task_definition
  launch_type      = "FARGATE"
  desired_count    = 1
  network_configuration {
    subnets = var.environment == "prd" ? ["subnet-xxxxxxxxxxxxxxxxx"] : ["subnet-yyyyyyyyyyyyyyyyy"]
    security_groups = var.environment == "prd" ? ["sg-xxxxxxxxxxxxxxxxx"] : ["sg-yyyyyyyyyyyyyyyyy"]
  }

  tags = merge(var.common_tags, each.value.specific_tags)
}